
// Section: Document Object Model (DOM)
// allows us to access or modify the properties of an html element in a webpage
// we will focus on use of DOM in managing forms.


// For selecting HTML elements will be using document.querySelector
// Syntax: document.querySelector("html element")
// the querySelector

const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const age = document.querySelector('#txt-age');
console.log(txtFirstName)
const name = document.querySelector(`.first-name`);
console.log(name)
const span = document.querySelectorAll("span");
console.log(span)
const text = document.querySelectorAll("input[text]");
console.log(text)



const div = document.querySelectorAll(".first-div>span")
// select 2nd span
const divspan = document.querySelector(".first-div>span")
console.log(div)
console.log(divspan)
console.log(div[1])
console.log(document.querySelector('.colorselector'))

// Section: Event Listeners
    // whenever a user interacts with a webpage, this action is considered as an event.

    // working with events is large part of creating interactivity in a webpage.

// The function use is "addEventListener", it takes two arguments
    // first argument is a string identifying an event.
    // second argument, function that the listener will trigger once the "specified event is triggered"

    let firstName="";
    let lastName="";
    const spanFullName = document.querySelector('#fullName')

    let colorSelector = document.querySelector('.colorselector')
    let pickedColor = "black";
// txtFirstName.addEventListener("keyup", (event)=>{
//         console.log(event.target.value);
//         document.querySelector('#fullName').textContent = event.target.value
// })


// txtLastName.addEventListener("keyup", (event)=>{
//     console.log(event);
// })

let lastNameChanged = function(){

     return lastName = txtLastName.value
}

let firstNameChanged = function(){
     return  firstName = txtFirstName.value
}

function combineFirstandLastName() {
   spanFullName.innerHTML = firstName + " " + lastName;
}

let changeFullNameColor= function(){

    spanFullName.style.color = colorSelector.value;

}


txtFirstName.addEventListener("change", firstNameChanged);
txtLastName.addEventListener("change", lastNameChanged);
txtLastName.addEventListener("change", combineFirstandLastName);
txtFirstName.addEventListener("change", combineFirstandLastName);


colorSelector.addEventListener("change", changeFullNameColor);






// In your discussion files do the following.
// Listen to an event when the last name's input is changed.
// Instead of anonymous functions for each of the event listener:
// Create a function that will update the span's contents based on the value of the first and last name input fields.
// Create a function that will change the text color of the spanFullName depending on the button selected.
// Instruct the event listeners to use the created function.
// Create a git repository named S47.
// Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// Add the link in Boodle.